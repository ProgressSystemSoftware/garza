import javax.swing.*;
import java.lang.reflect.Modifier;

public class BuscarCliente extends JFrame {
    private String[] listCombo = {"Selecccionar Colonia", "T1"};
    /** DATOS PERSONALES*/
    /*  ETIQUETAS */
    private JLabel lblTitulo    = Draw.labelDefault("Buscar Cliente Por:", Draw.tituloArial);
    private JLabel lblContrato  = Draw.labelDefault("Contrato:", Draw.normalArial);
    private JLabel lblNombre    = Draw.labelDefault("Nombre:", Draw.normalArial);
    private JLabel lblApellidoP = Draw.labelDefault("Apellido Paterno:", Draw.normalArial);
    private JLabel lblApellidoM = Draw.labelDefault("Apellido Materno:", Draw.normalArial);
    /*  INPUT */
    private JTextField txtContrato  = Draw.textDefault();
    private JTextField txtNombre    = Draw.textDefault();
    private JTextField txtApellidoP = Draw.textDefault();
    private JTextField txtApellidoM = Draw.textDefault();

    /** DATOS DOMICILIO */
    /*  ETIQUETAS */
    private JLabel lblCalle     = Draw.labelDefault("Calle:", Draw.normalArial);
    private JLabel lblNumeroC   = Draw.labelDefault("Numero:", Draw.normalArial);
    private JLabel lblColonia   = Draw.labelDefault("Colonia:", Draw.normalArial);
    private JLabel lblCiudad    = Draw.labelDefault("Ciudad:", Draw.normalArial);
    private JLabel lblPlaca     = Draw.labelDefault("Placa:", Draw.normalArial);
    /*  INPUT */
    private JTextField txtCalle     = Draw.textDefault();
    private JTextField txtNumeroC   = Draw.textDefault();
    private JComboBox  cmbColonia   = new JComboBox(listCombo);
    private JTextField txtCiudad    = Draw.textDefault();
    private JTextField txtPlaca     = Draw.textDefault();

    /** BUTTONS */
    private JButton btnBuscarContrato  = Draw.defaultButton("Buscar");
    private JButton btnBuscarNombre    = Draw.defaultButton("Buscar");
    private JButton btnBuscarDireccion = Draw.defaultButton("Buscar");
    private JButton btnBuscarPlaca     = Draw.defaultButton("Buscar");

    private JButton btnNuevo     = Draw.defaultButton("NUEVO");
    private JButton btnConsultar = Draw.defaultButton("CONSULTAR");
    private JButton btnModificar = Draw.defaultButton("MODIFICAR");
    private JButton btnImprimirContrato   = Draw.defaultButton("IMPRIMIR CONTRATO");
    private JButton btnImprimirTarjeta    = Draw.defaultButton("IMPRIMIR TARJETA");
    private JButton btnSalir     = Draw.defaultButton("SALIR");

    /** TABLE */
    private JScrollPane listaDeClientes = Draw.tableDefault();

    BuscarCliente() {
        super("Buscar Cliente");
        this.setSize(1000,650);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setVisible(true);
        this.setLayout(null);
        posicionarObjetos();
        agregarObjetos();
        accionesButtons();
    }

    private void accionesButtons() {
        btnSalir.addActionListener(e -> {
            System.exit(0);
        });
    }

    private void posicionarObjetos() {
        /* DATOS PERSONALES */
        lblTitulo.setBounds     (10, 10,200,18);
        lblContrato.setBounds   (10, 30,200,18);
        txtContrato.setBounds   (10, 50,120,25);
        btnBuscarContrato.setBounds     (140,50,70, 25);
        lblNombre.setBounds     (10, 75,200,18);
        txtNombre.setBounds     (10, 93,200,25);
        lblApellidoP.setBounds  (10,118,200,18);
        txtApellidoP.setBounds  (10,138,200,25);
        lblApellidoM.setBounds  (10,163,200,18);
        txtApellidoM.setBounds  (10,185,200,25);
        btnBuscarNombre.setBounds     (140,215,70, 25);

        /* DATOS DOMICILIO */
        lblCalle.setBounds      (10,240,200,18);
        txtCalle.setBounds      (10,258,200,25);
        lblNumeroC.setBounds    (10,285,200,18);
        txtNumeroC.setBounds    (10,305,200,25);
        lblColonia.setBounds    (10,335,200,18);
        cmbColonia.setBounds    (10,355,200,25);
        lblCiudad.setBounds     (10,380,200,18);
        txtCiudad.setBounds     (10,400,200,25);
        btnBuscarDireccion.setBounds     (140,430,70, 25);

        lblPlaca.setBounds      (10, 460,50,18);
        txtPlaca.setBounds      (50, 460,80,25);
        btnBuscarPlaca.setBounds(140,460,70,25);

        /* TABLA */
        listaDeClientes.setBounds(230,10, 600,600);

        /* BOTONES */
        btnNuevo.setBounds      (835,10,160,25);
        btnConsultar.setBounds  (835,40,160,25);
        btnModificar.setBounds  (835,80,160,25);
        btnImprimirContrato.setBounds   (835,515,160,25);
        btnImprimirTarjeta.setBounds    (835,545,160,25);
        btnSalir.setBounds      (835,585,160,25);
    }

    private void agregarObjetos() {
        /* ETIQUETAS */
        this.add(lblTitulo);
        this.add(lblContrato);
        this.add(lblNombre);
        this.add(lblApellidoP);
        this.add(lblApellidoM);
        this.add(lblCalle);
        this.add(lblNumeroC);
        this.add(lblColonia);
        this.add(lblCiudad);
        this.add(lblPlaca);

        /* INPUT */
        this.add(txtContrato);
        this.add(txtNombre);
        this.add(txtApellidoP);
        this.add(txtApellidoM);
        this.add(txtCalle);
        this.add(txtNumeroC);
        this.add(cmbColonia);
        this.add(txtCiudad);
        this.add(txtPlaca);

        /* BUTTONS */
        this.add(btnBuscarContrato);
        this.add(btnBuscarNombre);
        this.add(btnBuscarDireccion);
        this.add(btnBuscarPlaca);

        this.add(btnNuevo);
        this.add(btnConsultar);
        this.add(btnModificar);
        this.add(btnImprimirContrato);
        this.add(btnImprimirTarjeta);
        this.add(btnSalir);

        /* TABLE */
        this.add(listaDeClientes);
    }
}
