import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import Draw.ButtonDefatult;

import java.awt.*;

public class Draw {
    static Font tituloArial = new Font("Arial", Font.PLAIN, 18);

    static Font normalArial = new Font("Arial", Font.PLAIN, 12);
    static Font boldArial = new Font("Arial", Font.BOLD, 12);

    static JLabel labelDefault(String text) {
        JLabel temp = new JLabel(text);
        temp.setFont(normalArial);
        return temp;
    }

    static JLabel labelDefault(String text, Font fuente) {
        JLabel temp = new JLabel(text);
        temp.setFont(fuente);
        return temp;
    }

    static JTextField textDefault() {
        JTextField temp = new JTextField();
        temp.setFont(normalArial);
        return temp;
    }

    static JButton defaultButton(String title) {
        ButtonDefatult temp = new ButtonDefatult(title);
        temp.setForeground(Color.white);
        temp.setBackground(new Color(0x4B40A0));
        temp.setHoverBackgroundColor(new Color(0x7167A0));
        temp.setPressedBackgroundColor(new Color(0x4B40A0));
        return temp;
    }

    static JScrollPane  tableDefault() {
        JTable TablaCitas = new JTable();
        JTableHeader header = TablaCitas.getTableHeader();
        DefaultTableModel ModeloTablaCitas = new DefaultTableModel();
        header.setBackground(new Color(0x7DDE66));
        header.setBorder(null);

        ModeloTablaCitas.addColumn("");
        ModeloTablaCitas.addColumn("");
        ModeloTablaCitas.addColumn("");
        ModeloTablaCitas.addColumn("");
        ModeloTablaCitas.addColumn("");
        ModeloTablaCitas.addColumn("");
        ModeloTablaCitas.addColumn("");
        ModeloTablaCitas.addColumn("");

        for (int i = 0; i < 100; i++) {
            Object[] t = new Object[8];
            t[0] = ((i + 1) * 1000);
            t[1] = "Nombre";
            t[2] = "Apellido P";
            t[2] = "Apellido M";
            t[3] = "";
            t[4] = "";
            t[5] = "";
            t[6] = "";
            t[7] = "";
            ModeloTablaCitas.addRow(t);
        }

        TablaCitas.setModel(ModeloTablaCitas);

        TablaCitas.getColumnModel().getColumn(0).setPreferredWidth(50);
        TablaCitas.getColumnModel().getColumn(1).setPreferredWidth(50);
        TablaCitas.getColumnModel().getColumn(2).setPreferredWidth(150);
        TablaCitas.getColumnModel().getColumn(3).setPreferredWidth(50);
        TablaCitas.getColumnModel().getColumn(4).setPreferredWidth(50);
        TablaCitas.getColumnModel().getColumn(5).setPreferredWidth(50);
        TablaCitas.getColumnModel().getColumn(6).setPreferredWidth(50);
        TablaCitas.getColumnModel().getColumn(7).setPreferredWidth(50);

        return new JScrollPane(TablaCitas);
    }

}
