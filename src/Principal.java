import javax.swing.*;

class Principal extends JFrame {
    private JMenuBar menuPrincipal = new JMenuBar();

    Principal() {
        super("Menú Principal");
        this.setSize(500,500);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLayout(null);
        insertMenuPrincipal();
        this.setJMenuBar(menuPrincipal);

//        test.setBounds(10,10,150,30);

//        this.add(test);
        this.repaint();
        this.setVisible(true);
    }

    private void insertMenuPrincipal() {
        JMenu catalogos = new JMenu("Catalogos");
        JMenu procesos = new JMenu("Procesos");
        JMenu reportes = new JMenu("Reportes");
        JMenu ayuda = new JMenu("Ayuda");
        JMenu clientesMenu = new JMenu("Clientes");

        JMenuItem clientesItem = new JMenuItem("Clientes");
        JMenuItem areaTecnica = new JMenuItem("Área Tecnica");
        JMenuItem catalogoGenerales = new JMenuItem("Catálogo de Generales");
        JMenuItem ventas = new JMenuItem("Ventas");

        clientesMenu.add(clientesItem);

        catalogos.add(clientesMenu);
        catalogos.add(areaTecnica);
        catalogos.add(catalogoGenerales);
        catalogos.add(ventas);

        menuPrincipal.add(catalogos);
        menuPrincipal.add(procesos);
        menuPrincipal.add(reportes);
        menuPrincipal.add(ayuda);

        clientesItem.addActionListener(e -> {
            new BuscarCliente();
        });
    }
}
